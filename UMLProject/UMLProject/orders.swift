//
//  orders.swift
//  UMLProject
//
//  Created by Brian Jones on 2/6/18.
//  Copyright © 2018 Brian Jones. All rights reserved.
//

import Foundation

class orders:IDisplay {
    
    private var orderId:Int
    private var dateCreated:Date = Date()
    public private(set) var dateShipped:Date?
    private var cart:[orderDetails]
    
    private var shippingInf:[shippingInfo]
    
    private var orderTotal:Float = Float()
    
    init(){
        self.orderId = 0
        self.dateCreated = Date()
        self.dateShipped = Date()
        self.cart = [orderDetails]()
        
        self.shippingInf = [shippingInfo]()
        
        self.orderTotal = 0
    }
    
    init(orderId:Int, dateCreated:Date, shippingInf:[shippingInfo]) {
        self.orderId = orderId
        self.dateCreated = dateCreated
        self.cart = Array<orderDetails>()
        
        self.shippingInf = [shippingInfo]()
    }
    
    public func addToCart(cartItem:orderDetails) {
        cart.append(cartItem);
    }
    
    public func addShippingMethod(shippingMethod:shippingInfo) {
        shippingInf.append(shippingMethod);
    }
 
    private func getFormattedDate(date: Date) -> String
    {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE, dd MMMM, yyyy"
        let formattedDate = dateFormatterPrint.string(from: date)
        return formattedDate
    }
    
    func calculateOrderTotal() {
        var finalOrderTotal:Float = 0.0
        for products in cart {
            finalOrderTotal += products.getSubTotal()
        }
        self.orderTotal = finalOrderTotal
    }
    
    func shipOrder(){
        dateShipped = Calendar.current.date(byAdding: .day, value: 30, to: Date())
    }

    func removeProduct(productId:Int) {
        var arrayNewProducts:[orderDetails] = [orderDetails]()
        for product in self.cart {
            if product.productId != productId {
                arrayNewProducts.append(product)
            }
        }
        self.cart = arrayNewProducts
    }
    
    func display() -> String {
        
        var orderInfo = ""
        
        for shipping in shippingInf {
            orderInfo.append("-------Shipping Information-------\n");
            orderInfo.append(shipping.display());
        }
        
        orderInfo.append("""
            
        -----------Order Information------------
        Order ID: \(self.orderId)
        Order Date: \(self.getFormattedDate(date: self.dateCreated))
        ----------------------------------------

        """)
        
        if dateShipped != nil {
            orderInfo.append("This order was shipped on \(self.getFormattedDate(date: self.dateShipped!))\n") }
            else {
            orderInfo.append("This order hasn`t been shipped")
            }
        orderInfo.append("ProductID\t ProductName\t Quantity\t unitCost\t subTotal")
        for info in cart
        {
            orderInfo.append(info.display());
        }

        orderInfo.append("\nThe total is: \(self.orderTotal)")
        return orderInfo
    }
}
