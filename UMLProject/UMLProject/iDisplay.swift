//
//  iDisplay.swift
//  UMLProject
//
//  Created by Brian Jones on 2/6/18.
//  Copyright © 2018 Brian Jones. All rights reserved.
//

import Foundation

@objc public protocol IDisplay
{
    func display() -> String
}


