import Foundation
class Customer:IDisplay
{
    public private(set) var customerId: String = String()
    private var customerName: String = String()
    private var address: String = String()
    private var email: String = String()
    public private(set) var customerPassword :String = String()
    private var creditCardInfo: [String] = []
    private var myOrders:[orders]
    
    init()
    {
        self.customerId = "Idname"
        self.customerName = "Abi"
        self.address = "Cass Avenue, Scarborough"
        self.email = "abii@gmail.com"
        self.myOrders = [orders]()
    }
    
    public func addOrder(myOrder:orders) {
        myOrders.append(myOrder);
    }
    
    func register(customerId: String,customerName: String,address: String,email: String ,customerPassword : String)
    {
        self.customerId = customerId
        self.customerName = customerName
        self.address = address
        self.email = email
        self.customerPassword = customerPassword
    }
    
    func updateProfile(customerName: String,address: String,email: String)
    {
        self.customerName = customerName
        self.address = address
        self.email = email
    }
    
    func addCreditCard(Name: String , creditCardNumber: Int, Expiry: Date)
    {
        creditCardInfo = [Name, String(creditCardNumber), getFormattedDate(date: Expiry)]
    }
    
   func display() -> String
    {
        return """
        
        -----------Customer Information------------
        CustomerName: \(self.customerName)
        Address: \(self.address)
        Email: \(self.email)
        Credit Card Info: Name: \(creditCardInfo[0]), Card N: \(creditCardInfo[1]), Exp: \(creditCardInfo[2])
        -------------------------------------------
        
        """
    }
    
    
    private func getFormattedDate(date: Date) -> String
    {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMMM/yyyy"
        let formattedDate = dateFormatterPrint.string(from: date)
        return formattedDate
    }

}

