//
//  Administrator.swift
//  DemoReg
//
//  Created by Abita Shiney on 2/9/18.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import Foundation

class Administrator: IDisplay
{
    
    var adminName: String = String()
    var email: String = String()
    
    init()
    {
        self.adminName = "Sakshi"
        self.email = "sakshi@gmail.com"
    }
    func display() -> String
    {
       return "Admin name: \(self.adminName)  Admin email: \(self.email) "
    }
    func updateCatalog(adminName: String ,email: String)
    {
        self.adminName = adminName
        self.email = email
    }
}
