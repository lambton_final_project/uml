//
//  shippingInfo.swift
//  UMLProject
//
//  Created by Brian Jones on 2/6/18.
//  Copyright © 2018 Brian Jones. All rights reserved.
//

import Foundation

public class shippingInfo: IDisplay
{
    private var shippingId:Int
    private var shippingType:String
    private var shippingCost:Float
    private var shippingRegionalId:Int
    
    private var shippingStatus:choseStatus
    
    init(){
        self.shippingId = 1
        self.shippingType = "Standard 1-3 Business day"
        self.shippingCost = 0.00
        self.shippingRegionalId = 2
        
        self.shippingStatus = .notShipped
    }
    
    init(shippingId:Int, shippingType:String, shippingCost:Float, shippingRegionalId:Int)
    {
        self.shippingId = shippingId;
        self.shippingType = shippingType;
        self.shippingCost = shippingCost;
        self.shippingRegionalId = shippingRegionalId;
        
        self.shippingStatus = .onItsWay
    }
    
    func updateShippingInfo(shippingId: Int, shippingType:String, shippingCost:Float, shippingRegionalId: Int, shippingStatus:choseStatus){
        
        self.shippingId = shippingId;
        self.shippingType = shippingType;
        self.shippingCost = shippingCost;
        self.shippingRegionalId = shippingRegionalId;
        
        self.shippingStatus = shippingStatus
    }
    
    enum choseStatus:String {
        case delivered, onItsWay, notShipped;
        
        var answer: String {
            switch self {
            case .delivered: return "Item has been shipped"
            case .onItsWay: return "Item should arrive soon"
            case .notShipped: return "Oups, your item is still with us"
            }
        }
    }
    
    public func display() -> String
    {
        return """
        The Shipping ID: \(shippingId)
        The Shipping Status is: \(shippingStatus.answer)
        
        """;
    }
}
