//
//  orderDetails.swift
//  UMLProject
//
//  Created by Brian Jones on 2/6/18.
//  Copyright © 2018 Brian Jones. All rights reserved.
//

import Foundation

class orderDetails: IDisplay {

    public private(set) var productId:Int
    private var productName:String
    private var quantity:Int
    private var unitCost:Float
    private var subTotal:Float = Float()
    
    init(){
        productId = 0
        productName = "Set a name"
        quantity = 0
        unitCost = 0.00
        subTotal = 0.00
    }
    
    init(productId:Int, productName:String, quantity:Int, unitCost:Float){
        self.productId = productId
        self.productName = productName
        self.quantity = quantity
        self.unitCost = unitCost
        
        self.subTotal = calcPrice()
    }
    
    func calcPrice() -> Float{
        subTotal = Float(quantity) * unitCost;
        return subTotal;
    }
    
    func getSubTotal() -> Float {
        return subTotal
    }
    
    func updateQuantity(newQuantity:Int) {
        self.quantity = newQuantity
    }
    
    func display() -> String {
        return """
        \n\(self.productId)\t       \(self.productName)\t        \(self.unitCost)\t          \(self.subTotal)
        
        """
    }
    
}
